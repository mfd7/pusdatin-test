import 'package:bangunan/_core/constants/app_flavor.dart';
import 'package:bangunan/_core/my_app.dart';
import 'package:flutter/material.dart';
import 'package:bangunan/_core/injection.dart' as di;

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  di.init();
  AppFlavor(
    baseUrl: 'https://dev-carik.jakarta.go.id/api/pusdatin-service',
    flavor: Flavor.prod,
  );
  runApp(const MyApp());
}
