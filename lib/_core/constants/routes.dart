class Routes {
  // Splash Screen
  static const String splashRoute = '/';
  // Home Screen
  static const String homeRoute = '/home';
  // Create Screen
  static const String createRoute = '/create';
}
