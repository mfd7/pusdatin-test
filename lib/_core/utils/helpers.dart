import 'package:flutter/material.dart';

class Helpers {
  static Future<void> showErrorDialog(BuildContext context,
      {required String message, required Function onTryAgain}) async {
    if (ModalRoute.of(context)?.isCurrent != true) {
      return;
    }
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Error'),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text('Try Again'),
              onPressed: () {
                Navigator.of(context).pop();
                onTryAgain();
              },
            ),
          ],
        );
      },
    );
  }
}
