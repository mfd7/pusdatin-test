import 'package:bangunan/_core/constants/routes.dart';
import 'package:bangunan/presentation/screens/create/screens/create_screen.dart';
import 'package:bangunan/presentation/screens/home/screens/home_screen.dart';
import 'package:bangunan/presentation/screens/splash/screens/splash_screen.dart';
import 'package:flutter/material.dart';

class AppRouter {
  Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.splashRoute:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const SplashScreen(),
        );
      case Routes.homeRoute:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => const HomeScreen(),
        );
      case Routes.createRoute:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => CreateScreen(
            arguments: settings.arguments as CreateScreenArguments,
          ),
        );
      default:
        return MaterialPageRoute(
          builder: (_) => const Scaffold(
            body: Center(
              child: Text('Halaman tidak ditemukan'),
            ),
          ),
        );
    }
  }
}
