import 'package:bangunan/_core/constants/app_constant.dart';
import 'package:bangunan/data/_core/api_base_helper.dart';
import 'package:bangunan/data/_core/interceptors/auth_interceptor.dart';
import 'package:flutter_alice/alice.dart';
import 'package:get_it/get_it.dart';
import 'package:http_interceptor/http/intercepted_client.dart';

import 'package:http/http.dart' as http;
import 'package:bangunan/data/datasource_injection.dart'
    as datasource_injection;
import 'package:bangunan/data/repository_injection.dart'
    as repository_injection;
import 'package:bangunan/domain/usecase_injection.dart' as usecase_injection;
import 'package:bangunan/presentation/bloc_injection.dart' as bloc_injection;

final locator = GetIt.instance;

void init() {
  bloc_injection.init();
  usecase_injection.init();
  repository_injection.init();
  datasource_injection.init();

  // remote service
  locator.registerLazySingleton<AuthInterceptor>(
    () => AuthInterceptor(
      alice: locator(),
    ),
  );
  locator.registerLazySingleton<InterceptedClient>(
    () => InterceptedClient.build(
      requestTimeout: const Duration(seconds: AppConstant.timeoutSeconds),
      interceptors: [
        locator<AuthInterceptor>(),
      ],
    ),
  );
  locator.registerLazySingleton<ApiBaseHelper>(
    () => ApiBaseHelper(client: locator(), networkInspector: locator()),
  );

  locator.registerLazySingleton(() => http.Client());

  // Network inspector tool
  locator.registerLazySingleton<Alice>(() => Alice(showInspectorOnShake: true));
}
