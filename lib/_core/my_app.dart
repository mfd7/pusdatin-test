import 'package:bangunan/_core/app_router.dart';
import 'package:bangunan/_core/constants/routes.dart';
import 'package:bangunan/presentation/screens/create/bloc/edit/edit_bloc.dart';
import 'package:bangunan/presentation/screens/create/bloc/kelompok/kelompok_bloc.dart';
import 'package:bangunan/presentation/screens/create/bloc/submit/submit_bloc.dart';
import 'package:bangunan/presentation/screens/home/bloc/home_bloc.dart';
import 'package:bangunan/presentation/screens/splash/bloc/splash_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_alice/alice.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:bangunan/_core/injection.dart' as di;

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => di.locator<SplashBloc>()),
        BlocProvider(create: (_) => di.locator<HomeBloc>()),
        BlocProvider(create: (_) => di.locator<KelompokBloc>()),
        BlocProvider(create: (_) => di.locator<SubmitBloc>()),
        BlocProvider(create: (_) => di.locator<EditBloc>()),
      ],
      child: OverlaySupport.global(
        child: MaterialApp(
          navigatorKey: di.locator<Alice>().getNavigatorKey(),
          title: 'Bangunan',
          onGenerateRoute: AppRouter().generateRoute,
          initialRoute: Routes.splashRoute,
        ),
      ),
    );
  }
}
