import 'package:bangunan/data/bangunan/datasources/bangunan_remote_datasource.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerLazySingleton<BangunanRemoteDatasource>(
      () => BangunanRemoteDatasourceImpl(locator()));
}
