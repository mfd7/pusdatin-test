import 'package:bangunan/data/bangunan/repositories/bangunan_repository_impl.dart';
import 'package:bangunan/domain/bangunan/repositories/bangunan_repository.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerLazySingleton<BangunanRepository>(
      () => BangunanRepositoryImpl(locator()));
}
