import 'package:bangunan/data/_core/exception.dart';
import 'package:bangunan/data/_core/failure.dart';
import 'package:bangunan/data/bangunan/datasources/bangunan_remote_datasource.dart';
import 'package:bangunan/domain/bangunan/entities/kelompok.dart';
import 'package:bangunan/domain/bangunan/entities/list_bangunan.dart';
import 'package:bangunan/domain/bangunan/repositories/bangunan_repository.dart';
import 'package:dartz/dartz.dart';

class BangunanRepositoryImpl implements BangunanRepository {
  final BangunanRemoteDatasource bangunanRemoteDatasource;

  BangunanRepositoryImpl(this.bangunanRemoteDatasource);

  @override
  Future<Either<Failure, ListBangunan>> retrieveListBangunan(int page) async {
    try {
      final responses =
          await bangunanRemoteDatasource.retrieveListBangunan(page);
      return Right(responses.toEntity());
    } on BadRequestException catch (e) {
      return Left(RequestFailure(e.toString()));
    } on ServerErrorException {
      return const Left(ServerFailure());
    } on ParseDataException {
      return const Left(NoDataFailure());
    } on NoConnectionException {
      return const Left(ConnectionFailure());
    } on UnauthorisedException {
      return const Left(UnAuthorizedFailure());
    }
  }

  @override
  Future<Either<Failure, List<Kelompok>>> retrieveListKelompok() async {
    try {
      final responses = await bangunanRemoteDatasource.retrieveListKelompok();
      return Right(responses.map((e) => e.toEntity()).toList());
    } on BadRequestException catch (e) {
      return Left(RequestFailure(e.toString()));
    } on ServerErrorException {
      return const Left(ServerFailure());
    } on ParseDataException {
      return const Left(NoDataFailure());
    } on NoConnectionException {
      return const Left(ConnectionFailure());
    } on UnauthorisedException {
      return const Left(UnAuthorizedFailure());
    }
  }

  @override
  Future<Either<Failure, String>> submitBangunan(
      {required String name,
      required int noUrut,
      required int idKelompok,
      bool isEdit = false,
      int? idBangunan}) async {
    try {
      final responses = await bangunanRemoteDatasource.submitBangunan(
          name: name,
          noUrut: noUrut,
          idKelompok: idKelompok,
          isEdit: isEdit,
          idBangunan: idBangunan);
      return Right(responses);
    } on BadRequestException catch (e) {
      return Left(RequestFailure(e.toString()));
    } on ServerErrorException {
      return const Left(ServerFailure());
    } on ParseDataException {
      return const Left(NoDataFailure());
    } on NoConnectionException {
      return const Left(ConnectionFailure());
    } on UnauthorisedException {
      return const Left(UnAuthorizedFailure());
    }
  }
}
