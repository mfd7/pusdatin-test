import 'package:bangunan/domain/bangunan/entities/bangunan.dart';

class BangunanModel {
  BangunanModel({
    num? id,
    String? createdAt,
    String? updatedAt,
    String? createdBy,
    String? updatedBy,
    String? identifikasi,
    num? noUrut,
    num? idKelompok,
    dynamic bentuk,
    dynamic jenis,
    bool? deleted,
  }) {
    _id = id;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _createdBy = createdBy;
    _updatedBy = updatedBy;
    _identifikasi = identifikasi;
    _noUrut = noUrut;
    _idKelompok = idKelompok;
    _bentuk = bentuk;
    _jenis = jenis;
    _deleted = deleted;
  }

  BangunanModel.fromJson(dynamic json) {
    _id = json['id'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _createdBy = json['createdBy'];
    _updatedBy = json['updatedBy'];
    _identifikasi = json['identifikasi'];
    _noUrut = json['noUrut'];
    _idKelompok = json['idKelompok'];
    _bentuk = json['bentuk'];
    _jenis = json['jenis'];
    _deleted = json['deleted'];
  }
  num? _id;
  String? _createdAt;
  String? _updatedAt;
  String? _createdBy;
  String? _updatedBy;
  String? _identifikasi;
  num? _noUrut;
  num? _idKelompok;
  dynamic _bentuk;
  dynamic _jenis;
  bool? _deleted;
  BangunanModel copyWith({
    num? id,
    String? createdAt,
    String? updatedAt,
    String? createdBy,
    String? updatedBy,
    String? identifikasi,
    num? noUrut,
    num? idKelompok,
    dynamic bentuk,
    dynamic jenis,
    bool? deleted,
  }) =>
      BangunanModel(
        id: id ?? _id,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        createdBy: createdBy ?? _createdBy,
        updatedBy: updatedBy ?? _updatedBy,
        identifikasi: identifikasi ?? _identifikasi,
        noUrut: noUrut ?? _noUrut,
        idKelompok: idKelompok ?? _idKelompok,
        bentuk: bentuk ?? _bentuk,
        jenis: jenis ?? _jenis,
        deleted: deleted ?? _deleted,
      );
  num get id => _id ?? 0;
  String get createdAt => _createdAt ?? '';
  String get updatedAt => _updatedAt ?? '';
  String get createdBy => _createdBy ?? '';
  String get updatedBy => _updatedBy ?? '';
  String get identifikasi => _identifikasi ?? '';
  num get noUrut => _noUrut ?? 0;
  num get idKelompok => _idKelompok ?? 0;
  dynamic get bentuk => _bentuk;
  dynamic get jenis => _jenis;
  bool get deleted => _deleted ?? false;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['createdBy'] = _createdBy;
    map['updatedBy'] = _updatedBy;
    map['identifikasi'] = _identifikasi;
    map['noUrut'] = _noUrut;
    map['idKelompok'] = _idKelompok;
    map['bentuk'] = _bentuk;
    map['jenis'] = _jenis;
    map['deleted'] = _deleted;
    return map;
  }

  Bangunan toEntity() {
    return Bangunan(
      noUrut: noUrut.toInt(),
      name: identifikasi,
      idKelompok: idKelompok.toInt(),
      id: id.toInt(),
    );
  }
}
