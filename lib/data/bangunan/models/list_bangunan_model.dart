import 'package:bangunan/data/bangunan/models/bangunan_model.dart';
import 'package:bangunan/domain/bangunan/entities/list_bangunan.dart';

class ListBangunanModel {
  ListBangunanModel({
    num? total,
    List<BangunanModel>? data,
    num? size,
    num? totalPages,
    num? page,
  }) {
    _total = total;
    _data = data;
    _size = size;
    _totalPages = totalPages;
    _page = page;
  }

  ListBangunanModel.fromJson(dynamic json) {
    _total = json['total'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(BangunanModel.fromJson(v));
      });
    }
    _size = json['size'];
    _totalPages = json['totalPages'];
    _page = json['page'];
  }
  num? _total;
  List<BangunanModel>? _data;
  num? _size;
  num? _totalPages;
  num? _page;
  ListBangunanModel copyWith({
    num? total,
    List<BangunanModel>? data,
    num? size,
    num? totalPages,
    num? page,
  }) =>
      ListBangunanModel(
        total: total ?? _total,
        data: data ?? _data,
        size: size ?? _size,
        totalPages: totalPages ?? _totalPages,
        page: page ?? _page,
      );
  num get total => _total ?? 0;
  List<BangunanModel> get data => _data ?? [];
  num get size => _size ?? 0;
  num get totalPages => _totalPages ?? 0;
  num get page => _page ?? 0;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['total'] = _total;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['size'] = _size;
    map['totalPages'] = _totalPages;
    map['page'] = _page;
    return map;
  }

  ListBangunan toEntity() {
    return ListBangunan(
      listBangunan: data.map((e) => e.toEntity()).toList(),
      totalPages: totalPages.toInt(),
      currentPage: page.toInt(),
    );
  }
}
