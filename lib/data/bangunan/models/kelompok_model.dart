import 'package:bangunan/domain/bangunan/entities/kelompok.dart';

class KelompokModel {
  KelompokModel({
    num? id,
    String? createdAt,
    String? updatedAt,
    dynamic createdBy,
    String? updatedBy,
    String? name,
    String? nikKader,
    String? noKader,
    String? kodeRtKelompok,
    dynamic deleted,
    dynamic bangunanTarget,
    dynamic krtTarget,
    dynamic keluargaTarget,
    dynamic individuTarget,
    String? kodeLurahCapil,
    String? namaKader,
    String? labelRt,
    String? labelRw,
    dynamic questionerExist,
  }) {
    _id = id;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _createdBy = createdBy;
    _updatedBy = updatedBy;
    _name = name;
    _nikKader = nikKader;
    _noKader = noKader;
    _kodeRtKelompok = kodeRtKelompok;
    _deleted = deleted;
    _bangunanTarget = bangunanTarget;
    _krtTarget = krtTarget;
    _keluargaTarget = keluargaTarget;
    _individuTarget = individuTarget;
    _kodeLurahCapil = kodeLurahCapil;
    _namaKader = namaKader;
    _labelRt = labelRt;
    _labelRw = labelRw;
    _questionerExist = questionerExist;
  }

  KelompokModel.fromJson(dynamic json) {
    _id = json['id'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _createdBy = json['createdBy'];
    _updatedBy = json['updatedBy'];
    _name = json['name'];
    _nikKader = json['nikKader'];
    _noKader = json['noKader'];
    _kodeRtKelompok = json['kodeRtKelompok'];
    _deleted = json['deleted'];
    _bangunanTarget = json['bangunanTarget'];
    _krtTarget = json['krtTarget'];
    _keluargaTarget = json['keluargaTarget'];
    _individuTarget = json['individuTarget'];
    _kodeLurahCapil = json['kodeLurahCapil'];
    _namaKader = json['namaKader'];
    _labelRt = json['labelRt'];
    _labelRw = json['labelRw'];
    _questionerExist = json['questionerExist'];
  }
  num? _id;
  String? _createdAt;
  String? _updatedAt;
  dynamic _createdBy;
  String? _updatedBy;
  String? _name;
  String? _nikKader;
  String? _noKader;
  String? _kodeRtKelompok;
  dynamic _deleted;
  dynamic _bangunanTarget;
  dynamic _krtTarget;
  dynamic _keluargaTarget;
  dynamic _individuTarget;
  String? _kodeLurahCapil;
  String? _namaKader;
  String? _labelRt;
  String? _labelRw;
  dynamic _questionerExist;
  KelompokModel copyWith({
    num? id,
    String? createdAt,
    String? updatedAt,
    dynamic createdBy,
    String? updatedBy,
    String? name,
    String? nikKader,
    String? noKader,
    String? kodeRtKelompok,
    dynamic deleted,
    dynamic bangunanTarget,
    dynamic krtTarget,
    dynamic keluargaTarget,
    dynamic individuTarget,
    String? kodeLurahCapil,
    String? namaKader,
    String? labelRt,
    String? labelRw,
    dynamic questionerExist,
  }) =>
      KelompokModel(
        id: id ?? _id,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        createdBy: createdBy ?? _createdBy,
        updatedBy: updatedBy ?? _updatedBy,
        name: name ?? _name,
        nikKader: nikKader ?? _nikKader,
        noKader: noKader ?? _noKader,
        kodeRtKelompok: kodeRtKelompok ?? _kodeRtKelompok,
        deleted: deleted ?? _deleted,
        bangunanTarget: bangunanTarget ?? _bangunanTarget,
        krtTarget: krtTarget ?? _krtTarget,
        keluargaTarget: keluargaTarget ?? _keluargaTarget,
        individuTarget: individuTarget ?? _individuTarget,
        kodeLurahCapil: kodeLurahCapil ?? _kodeLurahCapil,
        namaKader: namaKader ?? _namaKader,
        labelRt: labelRt ?? _labelRt,
        labelRw: labelRw ?? _labelRw,
        questionerExist: questionerExist ?? _questionerExist,
      );
  num get id => _id ?? 0;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  dynamic get createdBy => _createdBy;
  String? get updatedBy => _updatedBy;
  String get name => _name ?? '';
  String? get nikKader => _nikKader;
  String? get noKader => _noKader;
  String? get kodeRtKelompok => _kodeRtKelompok;
  dynamic get deleted => _deleted;
  dynamic get bangunanTarget => _bangunanTarget;
  dynamic get krtTarget => _krtTarget;
  dynamic get keluargaTarget => _keluargaTarget;
  dynamic get individuTarget => _individuTarget;
  String? get kodeLurahCapil => _kodeLurahCapil;
  String? get namaKader => _namaKader;
  String? get labelRt => _labelRt;
  String? get labelRw => _labelRw;
  dynamic get questionerExist => _questionerExist;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['createdBy'] = _createdBy;
    map['updatedBy'] = _updatedBy;
    map['name'] = _name;
    map['nikKader'] = _nikKader;
    map['noKader'] = _noKader;
    map['kodeRtKelompok'] = _kodeRtKelompok;
    map['deleted'] = _deleted;
    map['bangunanTarget'] = _bangunanTarget;
    map['krtTarget'] = _krtTarget;
    map['keluargaTarget'] = _keluargaTarget;
    map['individuTarget'] = _individuTarget;
    map['kodeLurahCapil'] = _kodeLurahCapil;
    map['namaKader'] = _namaKader;
    map['labelRt'] = _labelRt;
    map['labelRw'] = _labelRw;
    map['questionerExist'] = _questionerExist;
    return map;
  }

  Kelompok toEntity() {
    return Kelompok(id: id.toInt(), name: name);
  }
}
