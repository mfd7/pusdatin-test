import 'package:bangunan/data/_core/api_base_helper.dart';
import 'package:bangunan/data/_core/exception.dart';
import 'package:bangunan/data/bangunan/bangunan_endpoints.dart';
import 'package:bangunan/data/bangunan/models/kelompok_model.dart';
import 'package:bangunan/data/bangunan/models/list_bangunan_model.dart';

abstract class BangunanRemoteDatasource {
  Future<ListBangunanModel> retrieveListBangunan(int page);
  Future<List<KelompokModel>> retrieveListKelompok();
  Future<String> submitBangunan(
      {required String name,
      required int noUrut,
      required int idKelompok,
      bool isEdit = false,
      int? idBangunan});
}

class BangunanRemoteDatasourceImpl implements BangunanRemoteDatasource {
  final ApiBaseHelper apiBaseHelper;

  BangunanRemoteDatasourceImpl(this.apiBaseHelper);

  @override
  Future<ListBangunanModel> retrieveListBangunan(int page) async {
    final response = await apiBaseHelper.getApi(BangunanEndpoints.listBangunan,
        qParams: {'page': page.toString()});
    try {
      return ListBangunanModel.fromJson(response);
    } catch (_) {
      throw ParseDataException();
    }
  }

  @override
  Future<List<KelompokModel>> retrieveListKelompok() async {
    final response = await apiBaseHelper.getApi(
      BangunanEndpoints.listKelompok,
    );
    try {
      List<dynamic> parsedListJson = response['data'];
      return List<KelompokModel>.from(
        parsedListJson.map<KelompokModel>(
          (i) => KelompokModel.fromJson(i),
        ),
      );
    } catch (_) {
      throw ParseDataException();
    }
  }

  @override
  Future<String> submitBangunan(
      {required String name,
      required int noUrut,
      required int idKelompok,
      bool isEdit = false,
      int? idBangunan}) async {
    dynamic response;
    if (isEdit) {
      response = await apiBaseHelper.putApi(
        BangunanEndpoints.createBangunan,
        body: {
          'id': idBangunan,
          'identifikasi': name,
          'noUrut': noUrut,
          'idKelompok': idKelompok
        },
        urlId: idBangunan.toString(),
      );
    } else {
      response = await apiBaseHelper.postApi(BangunanEndpoints.createBangunan,
          body: {
            'identifikasi': name,
            'noUrut': noUrut,
            'idKelompok': idKelompok
          });
    }
    try {
      return response['message'];
    } catch (_) {
      throw ParseDataException();
    }
  }
}
