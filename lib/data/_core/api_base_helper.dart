import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bangunan/_core/constants/app_flavor.dart';
import 'package:bangunan/_core/utils/log_util.dart';
import 'package:bangunan/data/_core/exception.dart';
import 'package:bangunan/data/_core/interceptors/auth_interceptor.dart';
import 'package:flutter_alice/alice.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/intercepted_client.dart';
import 'package:http_interceptor/http/interceptor_contract.dart';
import 'package:path/path.dart';

class ApiBaseHelper {
  final InterceptedClient client;
  final Alice? networkInspector;

  ApiBaseHelper({required this.client, required this.networkInspector});

  Future<dynamic> getApi(String url, {Map<String, String>? qParams}) async {
    final dynamic responseJson;
    try {
      final response = await client.get(
        Uri.parse('${AppFlavor.instance.baseUrl}$url')
            .replace(queryParameters: qParams),
      );
      responseJson = _returnResponse(response);
    } on HttpException {
      throw NoConnectionException();
    } on SocketException {
      throw NoConnectionException();
    } on TimeoutException {
      throw NoConnectionException();
    } on http.ClientException {
      throw NoConnectionException();
    }
    return responseJson;
  }

  Future<dynamic> postApi(
    String url, {
    required Map<String, dynamic> body,
    Map<String, String>? qParams,
  }) async {
    final dynamic responseJson;
    try {
      final response = await client.post(
        Uri.parse('${AppFlavor.instance.baseUrl}$url'),
        body: json.encode(body),
      );
      responseJson = _returnResponse(response);
    } on HttpException {
      throw NoConnectionException();
    } on SocketException {
      throw NoConnectionException();
    } on TimeoutException {
      throw NoConnectionException();
    } on http.ClientException {
      throw NoConnectionException();
    }
    return responseJson;
  }

  Future<dynamic> putApi(
    String url, {
    required Map<String, dynamic> body,
    Map<String, String>? qParams,
    required String urlId,
  }) async {
    final dynamic responseJson;
    try {
      final response = await client.put(
        Uri.parse('${AppFlavor.instance.baseUrl}$url/$urlId'),
        body: json.encode(body),
      );
      responseJson = _returnResponse(response);
    } on HttpException {
      throw NoConnectionException();
    } on SocketException {
      throw NoConnectionException();
    } on TimeoutException {
      throw NoConnectionException();
    } on http.ClientException {
      throw NoConnectionException();
    }
    return responseJson;
  }

  Future<dynamic> postMultipartApi(
    String url, {
    Map<String, String> body = const {},
    required Map<String, File> files,
  }) async {
    final dynamic responseJson;
    try {
      AuthInterceptor? authInterceptor;
      var request = http.MultipartRequest("POST", Uri.parse(url));
      request.fields.addAll(body);
      for (InterceptorContract interceptor in client.interceptors) {
        if (interceptor is AuthInterceptor) {
          authInterceptor = interceptor;
          var headers = await interceptor.generateHeaders();
          request.headers.addAll(headers);
        }
      }
      await Future.forEach(files.keys, (String key) async {
        final value = files[key];
        if (value != null) {
          final stream = http.ByteStream(value.openRead());
          stream.cast();
          final length = await value.length();
          final multipartFile = http.MultipartFile(key, stream, length,
              filename: basename(value.path));
          request.files.add(multipartFile);
        }
      });

      final logMessage = {
        "REQUEST KEY": request.headers['x-dynamic-token'],
        "REQUEST URL": request.url,
        "REQUEST METHOD": request.method,
        "REQUEST HEADER": request.headers,
        "REQUEST BODY": request.fields,
      };
      logger.i(logMessage);

      final response = await request.send();
      final responseFromStream = await http.Response.fromStream(response);
      final key = response.request?.headers['x-dynamic-token'] ?? '';
      String modifiedBody = responseFromStream.body;
      if (responseFromStream.body.isNotEmpty && authInterceptor != null) {
        var dataBody = jsonDecode(responseFromStream.body);
        // if (dataBody['data'] != null) {
        //   final decData = authInterceptor.decryptAES(dataBody['data']!, key);
        //   dataBody['data'] = jsonDecode(decData);
        // }
        modifiedBody = jsonEncode(dataBody);
      }
      final modifiedResponse = http.Response(
        modifiedBody,
        responseFromStream.statusCode,
        headers: responseFromStream.headers,
        isRedirect: responseFromStream.isRedirect,
        persistentConnection: responseFromStream.persistentConnection,
      );
      if (AppFlavor.isDev) {
        networkInspector?.onHttpResponse(responseFromStream, body: body);
      }
      responseJson = _returnResponse(modifiedResponse);
      var jsonBody = <String, dynamic>{};
      var jsonBodyEn = <String, dynamic>{};
      if (responseFromStream.body.isNotEmpty) {
        jsonBody = jsonDecode(responseFromStream.body);
      }
      if (modifiedResponse.body.isNotEmpty) {
        jsonBodyEn = jsonDecode(modifiedResponse.body);
      }
      final logResMessage = {
        "RESPONSE KEY": response.request?.headers['x-dynamic-token'],
        "RESPONSE URL": response.request?.url,
        "RESPONSE METHOD": response.request?.method,
        "RESPONSE CODE": response.statusCode,
        "RESPONSE DATA": jsonBody['data'],
        // "RESPONSE DATA(ENCRYPT)": jsonBodyEn['data'],
      };
      logger.i(logResMessage);
    } on SocketException {
      throw NoConnectionException();
    } on TimeoutException {
      throw NoConnectionException();
    }
    return responseJson;
  }

  dynamic _returnResponse(http.Response response) {
    if (jsonDecode(response.body)['status'] == false) {
      throw BadRequestException(
        jsonDecode(response.body)['message'],
      );
    }
    switch (response.statusCode) {
      case HttpStatus.ok:
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case HttpStatus.badRequest:
      case HttpStatus.notFound:
        if (response.body.isNotEmpty) {
          var responses = json.decode(response.body.toString());
          var getMessage = responses['message'];
          var isPinError = false;
          var isCardError = false;
          var isDeviceError = false;
          if (responses['pin_error'] != null &&
              responses['pin_error'] is bool) {
            isPinError = responses['pin_error'];
          }
          if (responses['card_error'] != null &&
              responses['card_error'] is bool) {
            isCardError = responses['card_error'];
          }
          if (responses['device_error'] != null &&
              responses['device_error'] is bool) {
            isDeviceError = responses['device_error'];
          }
          throw BadRequestException(
            getMessage,
            '',
            isPinError,
            isCardError,
            isDeviceError,
          );
        }
        throw BadRequestException(response.body.toString(), '');
      case HttpStatus.unauthorized:
      case HttpStatus.forbidden:
        throw UnauthorisedException(response.body.toString());
      case HttpStatus.internalServerError:
        throw ServerErrorException(response.body.toString());
      default:
        throw FetchDataException(
            'Error occured while communicating to server with statusCode : ${response.statusCode}');
    }
  }
}
