import 'dart:convert';

import 'package:bangunan/_core/constants/app_flavor.dart';
import 'package:bangunan/_core/utils/log_util.dart';
import 'package:flutter_alice/alice.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http_interceptor.dart';

class AuthInterceptor implements InterceptorContract {
  final Alice alice;

  AuthInterceptor({required this.alice});

  @override
  Future<RequestData> interceptRequest({required RequestData data}) async {
    var headers = await generateHeaders();
    /*if (kDebugMode) {
      var debugHeaders = <String, String>{'x-debug': "true"};
      headers.addAll(debugHeaders);
    }*/
    data.headers.addAll(headers);
    final logMessage = {
      "REQUEST KEY": data.headers['x-dynamic-token'],
      "REQUEST URL": data.url,
      "REQUEST METHOD": data.method,
      "REQUEST HEADER": data.headers,
    };

    if (data.body != null &&
        data.body is String &&
        (data.body as String).isNotEmpty) {
      logMessage['REQUEST BODY'] = jsonDecode(data.body);
      final key = data.headers['x-dynamic-token'] ?? '';
      var mapBody = <String, dynamic>{
        'request': data.body,
      };
      // data.body = jsonEncode(mapBody);
      logMessage['REQUEST BODY ENCRYPT'] = jsonDecode(data.body);
    }

    logger.i(logMessage);
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({required ResponseData data}) async {
    var jsonBody = <String, dynamic>{};
    if (data.body != null && data.body!.isNotEmpty) {
      if (jsonDecode(data.body ?? '').runtimeType == List<dynamic>) {
        jsonBody = {'list': jsonDecode(data.body ?? '')};
      } else {
        jsonBody = jsonDecode(data.body ?? '');
      }
    }
    final logMessage = {
      "RESPONSE KEY": data.request?.headers['x-dynamic-token'],
      "RESPONSE URL": data.url,
      "RESPONSE METHOD": data.method,
      "RESPONSE CODE": data.statusCode,
      "RESPONSE MESSAGE": jsonBody['message'],
      "RESPONSE DATA": jsonBody['data'],
    };

    final key = data.request?.headers['x-dynamic-token'] ?? '';
    if (data.body != null && data.body!.isNotEmpty) {
      var dataBody = jsonDecode(data.body!);
      // if (dataBody['data'] != null) {
      //   final decData = decryptAES(dataBody['data']!, key);
      //   dataBody['data'] = jsonDecode(decData);
      // }
      data.body = jsonEncode(dataBody);
    }

    final modifiedResponse = http.Response(
      data.body ?? '',
      data.statusCode,
      request: data.request?.toHttpRequest(),
      headers: data.headers ?? {},
      isRedirect: data.isRedirect ?? false,
      persistentConnection: data.persistentConnection ?? true,
    );
    var jsonBodyEn = <String, dynamic>{};
    if (data.body != null && data.body!.isNotEmpty) {
      if (jsonDecode(modifiedResponse.body).runtimeType == List<dynamic>) {
        jsonBodyEn = {'list': jsonDecode(modifiedResponse.body)};
      } else {
        jsonBodyEn = jsonDecode(modifiedResponse.body);
      }
    }
    logMessage["RESPONSE DATA DECRYPT"] = jsonBodyEn['data'];
    if (AppFlavor.isDev) {
      var aliceModifiedResponse = modifiedResponse;
      if (data.request != null) {
        final modifiedRequest = data.request!;
        if (modifiedRequest.body != null) {
          final key = modifiedRequest.headers['x-dynamic-token'] ?? '';
          var mapBody = {};
          if (modifiedRequest.body != null &&
              modifiedRequest.body is String &&
              (modifiedRequest.body as String).isNotEmpty) {
            mapBody = jsonDecode(modifiedRequest.body);
          }
          if (mapBody['request'] != null) {
            final requestBody = mapBody['request'];
            modifiedRequest.body = requestBody;
            aliceModifiedResponse = http.Response(
              data.body ?? '',
              data.statusCode,
              request: modifiedRequest.toHttpRequest(),
              headers: data.headers ?? {},
              isRedirect: data.isRedirect ?? false,
              persistentConnection: data.persistentConnection ?? true,
            );
          }
        }
      }
      alice.onHttpResponse(aliceModifiedResponse);
    }
    logger.i(logMessage);

    return ResponseData.fromHttpResponse(modifiedResponse);
  }

  Future<Map<String, String>> generateHeaders() async {
    // final token = await localDataSource.loadUserAccessToken() ?? '';
    // final location = await localDataSource.loadUserLocation() ?? '';
    // final deviceId = await PlatformDeviceId.getDeviceId ?? '';
    var headers = <String, String>{
      'content-type': 'application/json',
      // 'Authorization': 'Bearer ${AppConstant.kToken}',
      // 'x-dynamic-token': _generateRandomString(32),
      // 'x-access-token': 'Bearer $token',
      // 'x-device-id': deviceId,
      // 'x-location-address': location,
      // 'timestamp': getUnixTimeStamp(),
      // 'idempotency-key': generateIdempotencyKey(),
      // 'platform': 'mobile-apps',
      // 'timezone': DateTime.now().timeZoneOffset.inHours.toString(),
    };
    // if (!kReleaseMode) {
    //   var debugHeaders = <String, String>{'x-debug': "true"};
    //   headers.addAll(debugHeaders);
    // }

    return headers;
  }
}
