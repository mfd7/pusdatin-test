import 'package:bangunan/domain/bangunan/usecases/retrieve_list_bangunan_usecase.dart';
import 'package:bangunan/domain/bangunan/usecases/retrieve_list_kelompok_usecase.dart';
import 'package:bangunan/domain/bangunan/usecases/submit_bangunan_usecase.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerLazySingleton(() => RetrieveListBangunanUsecase(locator()));
  locator.registerLazySingleton(() => RetrieveListKelompokUsecase(locator()));
  locator.registerLazySingleton(() => SubmitBangunanUsecase(locator()));
}
