import 'package:bangunan/data/_core/failure.dart';
import 'package:bangunan/domain/bangunan/entities/kelompok.dart';
import 'package:bangunan/domain/bangunan/entities/list_bangunan.dart';
import 'package:dartz/dartz.dart';

abstract class BangunanRepository {
  Future<Either<Failure, ListBangunan>> retrieveListBangunan(int page);
  Future<Either<Failure, List<Kelompok>>> retrieveListKelompok();
  Future<Either<Failure, String>> submitBangunan(
      {required String name,
      required int noUrut,
      required int idKelompok,
      bool isEdit = false, int? idBangunan});
}
