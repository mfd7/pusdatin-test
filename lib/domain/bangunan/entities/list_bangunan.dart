import 'package:bangunan/domain/bangunan/entities/bangunan.dart';

class ListBangunan {
  final List<Bangunan> listBangunan;
  final int totalPages;
  final int currentPage;

  ListBangunan(
      {this.listBangunan = const [],
      this.totalPages = 0,
      this.currentPage = 0});

  ListBangunan copyWith({
    List<Bangunan>? listBangunan,
    int? totalPages,
    int? currentPage,
  }) {
    return ListBangunan(
        listBangunan: listBangunan ?? this.listBangunan,
        totalPages: totalPages ?? this.totalPages,
        currentPage: currentPage ?? this.currentPage);
  }
}
