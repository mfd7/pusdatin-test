class Bangunan {
  final int noUrut;
  final String name;
  final int idKelompok;
  final int? id;

  Bangunan({this.noUrut = 0, this.name = '', this.idKelompok = 0, this.id});
}
