import 'package:bangunan/data/_core/failure.dart';
import 'package:bangunan/domain/bangunan/entities/list_bangunan.dart';
import 'package:bangunan/domain/bangunan/repositories/bangunan_repository.dart';
import 'package:dartz/dartz.dart';

class RetrieveListBangunanUsecase {
  final BangunanRepository bangunanRepository;

  RetrieveListBangunanUsecase(this.bangunanRepository);

  Future<Either<Failure, ListBangunan>> execute(int page) async {
    return await bangunanRepository.retrieveListBangunan(page);
  }
}
