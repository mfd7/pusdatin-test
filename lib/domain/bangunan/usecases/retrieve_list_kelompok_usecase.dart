import 'package:bangunan/data/_core/failure.dart';
import 'package:bangunan/domain/bangunan/entities/kelompok.dart';
import 'package:bangunan/domain/bangunan/repositories/bangunan_repository.dart';
import 'package:dartz/dartz.dart';

class RetrieveListKelompokUsecase {
  final BangunanRepository bangunanRepository;

  RetrieveListKelompokUsecase(this.bangunanRepository);
  Future<Either<Failure, List<Kelompok>>> execute() async {
    return await bangunanRepository.retrieveListKelompok();
  }
}
