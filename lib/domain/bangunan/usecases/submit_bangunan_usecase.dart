import 'package:bangunan/data/_core/failure.dart';
import 'package:bangunan/domain/bangunan/repositories/bangunan_repository.dart';
import 'package:dartz/dartz.dart';

class SubmitBangunanUsecase {
  final BangunanRepository bangunanRepository;

  SubmitBangunanUsecase(this.bangunanRepository);
  Future<Either<Failure, String>> execute(
      {required String name,
      required int noUrut,
      required int idKelompok,
      bool isEdit = false,
      int? idBangunan}) async {
    return await bangunanRepository.submitBangunan(
        name: name,
        noUrut: noUrut,
        idKelompok: idKelompok,
        idBangunan: idBangunan,
        isEdit: isEdit);
  }
}
