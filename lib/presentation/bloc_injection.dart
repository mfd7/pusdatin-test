import 'package:bangunan/presentation/screens/create/bloc/edit/edit_bloc.dart';
import 'package:bangunan/presentation/screens/create/bloc/kelompok/kelompok_bloc.dart';
import 'package:bangunan/presentation/screens/create/bloc/submit/submit_bloc.dart';
import 'package:bangunan/presentation/screens/home/bloc/home_bloc.dart';
import 'package:bangunan/presentation/screens/splash/bloc/splash_bloc.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerFactory(() => SplashBloc());
  locator.registerFactory(() => HomeBloc(locator()));
  locator.registerFactory(() => KelompokBloc(locator()));
  locator.registerFactory(() => SubmitBloc(locator()));
  locator.registerFactory(() => EditBloc(locator()));
}
