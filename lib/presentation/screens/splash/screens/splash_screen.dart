import 'package:bangunan/_core/constants/routes.dart';
import 'package:bangunan/_core/utils/log_util.dart';
import 'package:bangunan/presentation/_core/app_color.dart';
import 'package:bangunan/presentation/_core/app_text_style.dart';
import 'package:bangunan/presentation/_core/widgets/dialog/error_dialog.dart';
import 'package:bangunan/presentation/screens/splash/bloc/splash_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    context.read<SplashBloc>().add(const OnScreenLoadedSplash());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashBloc, SplashState>(
      listener: (context, state) {
        if (state is SplashLoaded) {
          Navigator.of(context).pushReplacementNamed(Routes.homeRoute);
        }
        if (state is SplashError) {
          showErrorDialog(
              context: context,
              title: 'Error',
              message: state.message,
              buttonText: 'Try Again',
              onButtonCLick: () {
                context.read<SplashBloc>().add(const OnScreenLoadedSplash());
              });
        }
      },
      child: Scaffold(
        body: BlocBuilder<SplashBloc, SplashState>(
          builder: (context, state) {
            logger.e(state);
            if (state is SplashLoading) {
              return const LoadingSplash(
                message: 'Loading your settings',
              );
            }

            return Container();
          },
        ),
      ),
    );
  }
}

class LoadingSplash extends StatelessWidget {
  const LoadingSplash({Key? key, this.message}) : super(key: key);
  final String? message;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Stack(
            children: [
              CircularProgressIndicator(
                backgroundColor: AppColor.celestialBlue,
              ),
              CircularProgressIndicator(
                strokeWidth: 6.0,
                backgroundColor: Colors.transparent,
                valueColor: AlwaysStoppedAnimation<Color>(
                  AppColor.mediumPersianBlue,
                ),
              ),
            ],
          ),
          if (message != null && message!.isNotEmpty) ...[
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20.0,
                vertical: 20,
              ),
              child: Text(
                message!,
                style: AppTextStyle.bodyLarge.copyWith(
                  color: AppColor.neutral8,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ],
      ),
    );
  }
}

class ErrorSplash extends StatelessWidget {
  const ErrorSplash({
    Key? key,
    required this.message,
    required this.buttonText,
    this.onTap,
  }) : super(key: key);
  final String message;
  final String buttonText;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            child: Icon(
              Icons.warning_amber,
              color: Colors.amber,
              size: 40,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Text(
              message,
              style: AppTextStyle.bodyLarge.copyWith(
                color: AppColor.neutral8,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          InkWell(
            onTap: onTap,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              child: Text(
                buttonText,
                style: AppTextStyle.titleMedium.copyWith(
                  color: AppColor.celestialBlue,
                  decoration: TextDecoration.underline,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
