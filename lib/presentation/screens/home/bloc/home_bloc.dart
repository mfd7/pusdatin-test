import 'package:bangunan/domain/bangunan/entities/list_bangunan.dart';
import 'package:bangunan/domain/bangunan/usecases/retrieve_list_bangunan_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final RetrieveListBangunanUsecase _bangunanUsecase;
  int page = 1;
  bool hasNext = true;

  HomeBloc(this._bangunanUsecase) : super(HomeInitial()) {
    on<OnRetrieveListBangunan>((event, emit) async {
      if (!event.nextPage || !hasNext) {
        page = 1;
        emit(HomeLoading());
      } else {
        page++;
      }
      ListBangunan listBangunan = ListBangunan();
      final retrieve = await _bangunanUsecase.execute(page);
      retrieve.fold((failure) {
        emit(HomeFailure(failure.message));
      }, (data) {
        if (event.nextPage && page < data.totalPages) {
          listBangunan = (state as HomeLoaded).listBangunan.copyWith(
              listBangunan:
                  List.of((state as HomeLoaded).listBangunan.listBangunan)
                    ..addAll(data.listBangunan));
          if (page == data.totalPages) {
            hasNext = false;
          }
        } else {
          listBangunan = data;
        }
        emit(HomeLoaded(listBangunan));
      });
    });
    on<OnRefreshListBangunan>((event, emit) async {
      page = 1;
      hasNext = true;
      add(OnRetrieveListBangunan(false));
    });
  }
}
