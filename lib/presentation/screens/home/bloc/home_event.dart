part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class OnRetrieveListBangunan extends HomeEvent {
  final bool nextPage;

  OnRetrieveListBangunan(this.nextPage);
}

class OnRefreshListBangunan extends HomeEvent {}
