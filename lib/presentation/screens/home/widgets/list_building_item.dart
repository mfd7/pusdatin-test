import 'package:bangunan/_core/constants/app_constant.dart';
import 'package:bangunan/domain/bangunan/entities/bangunan.dart';
import 'package:bangunan/presentation/_core/app_color.dart';
import 'package:bangunan/presentation/_core/app_text_style.dart';
import 'package:flutter/material.dart';

class ListBuildingItem extends StatelessWidget {
  const ListBuildingItem({
    super.key,
    required this.bangunan,
    required this.onTap,
  });

  final Bangunan bangunan;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Ink(
        width: double.infinity,
        padding: const EdgeInsets.all(AppConstant.kDefaultPadding / 2),
        decoration: BoxDecoration(
          color: AppColor.neutral1,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: AppColor.neutral8.withOpacity(0.5),
              offset: const Offset(1, 1),
              blurRadius: 2,
              spreadRadius: 1,
            )
          ],
        ),
        child: Row(
          children: [
            const Icon(
              Icons.house_rounded,
              size: 80,
              color: AppColor.primary500,
            ),
            const SizedBox(
              width: AppConstant.kDefaultPadding / 2,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'No. Urut Bangunan ${bangunan.noUrut}',
                  style: AppTextStyle.bodyMedium.copyWith(
                    color: AppColor.primary900,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  bangunan.name,
                  style: AppTextStyle.bodyMedium.copyWith(
                    color: AppColor.primary900,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
