import 'package:bangunan/_core/constants/app_constant.dart';
import 'package:bangunan/domain/bangunan/entities/bangunan.dart';
import 'package:bangunan/presentation/_core/app_text_style.dart';
import 'package:bangunan/presentation/_core/widgets/custom_button.dart';
import 'package:flutter/material.dart';

class DetailBottomSheet extends StatelessWidget {
  const DetailBottomSheet(
      {super.key, required this.bangunan, required this.onTap});

  final Bangunan bangunan;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 150,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.center,
            child: Text(
              bangunan.name,
              style: AppTextStyle.titleLarge,
            ),
          ),
          const SizedBox(
            height: AppConstant.kDefaultPadding,
          ),
          Text(
            'No. urut bangunan ${bangunan.noUrut}',
            style: AppTextStyle.bodyLarge.copyWith(fontWeight: FontWeight.w400),
          ),
          const SizedBox(
            height: AppConstant.kDefaultPadding * 2,
          ),
          SizedBox(
            width: double.infinity,
            child: CustomButton.solidPrimaryLarge(
              onTap: onTap,
              text: 'Edit',
            ),
          )
        ],
      ),
    );
  }
}
