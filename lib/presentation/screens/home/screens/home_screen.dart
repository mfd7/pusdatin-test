import 'package:bangunan/_core/constants/app_constant.dart';
import 'package:bangunan/_core/constants/routes.dart';
import 'package:bangunan/_core/utils/helpers.dart';
import 'package:bangunan/domain/bangunan/entities/bangunan.dart';
import 'package:bangunan/presentation/_core/app_color.dart';
import 'package:bangunan/presentation/_core/app_text_style.dart';
import 'package:bangunan/presentation/_core/widgets/custom_shimmer.dart';
import 'package:bangunan/presentation/_core/widgets/regular_bottom_sheet.dart';
import 'package:bangunan/presentation/screens/create/screens/create_screen.dart';
import 'package:bangunan/presentation/screens/home/bloc/home_bloc.dart';
import 'package:bangunan/presentation/screens/home/widgets/detail_bottom_sheet.dart';
import 'package:bangunan/presentation/screens/home/widgets/list_building_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    context.read<HomeBloc>().add(OnRetrieveListBangunan(false));
    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_onScroll);
    super.dispose();
  }

  void _onScroll() {
    if (_isBottom) {
      context.read<HomeBloc>().add(OnRetrieveListBangunan(true));
    }
  }

  bool get _isBottom {
    return _scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent;
  }

  onRefresh() {
    context.read<HomeBloc>().add(OnRefreshListBangunan());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      listener: (context, state) {
        if (state is HomeFailure) {
          Helpers.showErrorDialog(context,
              message: state.message, onTryAgain: () {});
        }
      },
      child: Scaffold(
        backgroundColor: AppColor.neutral1,
        appBar: AppBar(
          backgroundColor: AppColor.primary500,
          title: Text(
            'Bangunan',
            style: AppTextStyle.titleLarge.copyWith(color: AppColor.neutral1),
          ),
        ),
        body: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            if (state is HomeLoaded) {
              return RefreshIndicator(
                onRefresh: () {
                  onRefresh();
                  return Future(() => true);
                },
                child: ListView.separated(
                  controller: _scrollController,
                  padding: const EdgeInsets.symmetric(
                      horizontal: AppConstant.kDefaultPadding,
                      vertical: AppConstant.kDefaultPadding / 2),
                  itemBuilder: (BuildContext context, int index) {
                    if (index == state.listBangunan.listBangunan.length) {
                      return const Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: AppConstant.kDefaultPadding * 2),
                        child: Center(child: CircularProgressIndicator()),
                      );
                    }
                    final item =
                        state.listBangunan.listBangunan.elementAt(index);
                    return ListBuildingItem(
                      bangunan: item,
                      onTap: () {
                        showRegularModalBottomSheet(
                          context: context,
                          content: DetailBottomSheet(
                              bangunan: item,
                              onTap: () async {
                                Navigator.pop(context);
                                final isSubmit = await Navigator.of(context)
                                    .pushNamed(Routes.createRoute,
                                        arguments: CreateScreenArguments(
                                            isEdit: true, bangunan: item));
                                if (isSubmit != null) {
                                  if (isSubmit as bool && context.mounted) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                          content: Text(
                                              'Bangunan berhasil ditambahkan!')),
                                    );
                                    onRefresh();
                                  }
                                }
                              }),
                        );
                      },
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(
                      height: AppConstant.kDefaultPadding / 2,
                    );
                  },
                  itemCount: state.listBangunan.listBangunan.length + 1,
                ),
              );
            }
            if (state is HomeLoading) {
              return ListView(
                padding: const EdgeInsets.symmetric(
                    horizontal: AppConstant.kDefaultPadding,
                    vertical: AppConstant.kDefaultPadding / 2),
                children: List.generate(
                  10,
                  (index) => const CustomShimmer(
                    child: Padding(
                      padding: EdgeInsets.only(
                          bottom: AppConstant.kDefaultPadding / 2),
                      child: TextPlaceHolder(
                        height: 100,
                      ),
                    ),
                  ),
                ),
              );
            }
            return Container();
          },
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: AppColor.secondaryNormal,
          child: const Icon(
            Icons.add,
            color: AppColor.neutral1,
          ),
          onPressed: () async {
            final isSubmit = await Navigator.of(context).pushNamed(
                Routes.createRoute,
                arguments:
                    CreateScreenArguments(isEdit: false, bangunan: Bangunan()));
            if (isSubmit != null) {
              if (isSubmit as bool && context.mounted) {
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                      content: Text('Bangunan berhasil ditambahkan!')),
                );
                onRefresh();
              }
            }
          },
        ),
      ),
    );
  }
}
