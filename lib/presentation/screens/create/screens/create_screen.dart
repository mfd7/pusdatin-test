import 'package:bangunan/_core/constants/app_constant.dart';
import 'package:bangunan/domain/bangunan/entities/bangunan.dart';
import 'package:bangunan/presentation/_core/app_color.dart';
import 'package:bangunan/presentation/_core/app_text_style.dart';
import 'package:bangunan/presentation/_core/widgets/custom_button.dart';
import 'package:bangunan/presentation/_core/widgets/custom_dropdown.dart';
import 'package:bangunan/presentation/_core/widgets/custom_text_field.dart';
import 'package:bangunan/presentation/_core/widgets/dialog/confirmation_dialog.dart';
import 'package:bangunan/presentation/_core/widgets/dialog/error_dialog.dart';
import 'package:bangunan/presentation/_core/widgets/dialog/processing_dialog.dart';
import 'package:bangunan/presentation/screens/create/bloc/edit/edit_bloc.dart';
import 'package:bangunan/presentation/screens/create/bloc/kelompok/kelompok_bloc.dart';
import 'package:bangunan/presentation/screens/create/bloc/submit/submit_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CreateScreenArguments {
  final bool isEdit;
  final Bangunan bangunan;

  CreateScreenArguments({required this.isEdit, required this.bangunan});
}

class CreateScreen extends StatefulWidget {
  const CreateScreen({super.key, required this.arguments});
  final CreateScreenArguments arguments;

  @override
  State<CreateScreen> createState() => _CreateScreenState();
}

class _CreateScreenState extends State<CreateScreen> {
  final namaBangunanController = TextEditingController();
  final namaBangunanFocus = FocusNode();
  final noUrutBangunanController = TextEditingController();
  final noUrutBangunanFocus = FocusNode();
  final listKelompokFocus = FocusNode();
  bool isValid = false;
  int kelompok = 0;
  int status = 0;

  @override
  void initState() {
    super.initState();
    context.read<KelompokBloc>().add(OnRetrieveListKelompok());
    addFocusListeners();
  }

  @override
  void dispose() {
    super.dispose();
    namaBangunanFocus.removeListener(() {});
    noUrutBangunanFocus.removeListener(() {});
    listKelompokFocus.removeListener(() {});
    namaBangunanFocus.dispose();
    noUrutBangunanFocus.dispose();
    listKelompokFocus.dispose();
  }

  fillData() {
    namaBangunanController.text = widget.arguments.bangunan.name;
    noUrutBangunanController.text = widget.arguments.bangunan.noUrut.toString();
    kelompok = widget.arguments.bangunan.idKelompok;
  }

  addFocusListeners() {
    namaBangunanFocus.addListener(() {
      setState(() {});
    });
    noUrutBangunanFocus.addListener(() {
      setState(() {});
    });
    listKelompokFocus.addListener(() {
      setState(() {});
    });
  }

  checkValidity() {
    if (namaBangunanController.text.isNotEmpty &&
        namaBangunanController.text.isNotEmpty &&
        kelompok != 0) {
      setState(() {
        isValid = true;
      });
    } else {
      setState(() {
        isValid = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<KelompokBloc, KelompokState>(listener: (context, state) {
          if (state is KelompokLoading) {
            showProcessingDialog(context: context, title: 'Fetching Kelompok');
          }
          if (state is KelompokLoaded) {
            Navigator.pop(context);
            fillData();
          }
        }),
        BlocListener<SubmitBloc, SubmitState>(listener: (context, state) {
          if (state is SubmitLoading) {
            showProcessingDialog(context: context, title: 'Submitting');
          }
          if (state is SubmitSuccess) {
            Navigator.pop(context);
            Navigator.pop(context, true);
          }
          if (state is SubmitFailure) {
            Navigator.pop(context);
            showErrorDialog(
                context: context,
                message: state.message,
                title: 'Error',
                buttonText: 'OK');
          }
        }),
        BlocListener<EditBloc, EditState>(listener: (context, state) {
          if (state is EditLoading) {
            showProcessingDialog(context: context, title: 'Submitting');
          }
          if (state is EditSuccess) {
            Navigator.pop(context);
            Navigator.pop(context, true);
          }
          if (state is EditFailure) {
            Navigator.pop(context);
            showErrorDialog(
                context: context,
                message: state.message,
                title: 'Error',
                buttonText: 'OK');
          }
        })
      ],
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppColor.neutral1,
        appBar: AppBar(
          iconTheme: const IconThemeData(
            color: AppColor.neutral1,
          ),
          backgroundColor: AppColor.primary500,
          title: Text(
            widget.arguments.isEdit ? 'Edit Bangunan' : 'Create',
            style: AppTextStyle.titleLarge.copyWith(color: AppColor.neutral1),
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                keyboardDismissBehavior:
                    ScrollViewKeyboardDismissBehavior.onDrag,
                padding: const EdgeInsets.symmetric(
                    horizontal: AppConstant.kDefaultPadding,
                    vertical: AppConstant.kDefaultPadding / 2),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomTextField(
                      inputTitle: 'Nama Bangunan',
                      controller: namaBangunanController,
                      focusNode: namaBangunanFocus,
                      isFocus: namaBangunanFocus.hasFocus,
                      onChanged: (value) {
                        checkValidity();
                      },
                    ),
                    const SizedBox(
                      height: AppConstant.kDefaultPadding,
                    ),
                    CustomTextField(
                      inputTitle: 'No. Urut Bangunan',
                      controller: noUrutBangunanController,
                      focusNode: noUrutBangunanFocus,
                      isFocus: noUrutBangunanFocus.hasFocus,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp("[0-9]")),
                      ],
                      onChanged: (value) {
                        checkValidity();
                      },
                    ),
                    const SizedBox(
                      height: AppConstant.kDefaultPadding,
                    ),
                    Text(
                      'Status Bangunan',
                      style: AppTextStyle.bodyLarge.copyWith(
                        color: AppColor.neutral10,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'Berpenghuni',
                        style: AppTextStyle.bodyLarge,
                      ),
                      leading: Radio(
                        value: 0,
                        groupValue: status,
                        onChanged: (value) {
                          if (value != null) {
                            setState(() {
                              status = value;
                            });
                          }
                        },
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'Tidak Berpenghuni',
                        style: AppTextStyle.bodyLarge,
                      ),
                      leading: Radio(
                        value: 1,
                        groupValue: status,
                        onChanged: (value) {
                          if (value != null) {
                            setState(() {
                              status = value;
                            });
                          }
                        },
                      ),
                    ),
                    const SizedBox(
                      height: AppConstant.kDefaultPadding,
                    ),
                    BlocBuilder<KelompokBloc, KelompokState>(
                      builder: (context, state) {
                        if (state is KelompokLoaded) {
                          return CustomDropdown(
                            focusNode: listKelompokFocus,
                            isFocus: listKelompokFocus.hasFocus,
                            items: state.listKelompok
                                .map((e) => DropdownMenuItem(
                                      value: e.id,
                                      child: Text(e.name),
                                    ))
                                .toList(),
                            selectedItem: kelompok != 0 ? kelompok : null,
                            onChanged: (value) {
                              setState(() {
                                kelompok = value;
                              });
                              checkValidity();
                            },
                            inputTitle: 'Pilih Kelompok',
                            hint: '',
                          );
                        }
                        return CustomDropdown(
                          focusNode: listKelompokFocus,
                          isFocus: listKelompokFocus.hasFocus,
                          items: const [],
                          selectedItem: null,
                          onChanged: (value) {
                            checkValidity();
                          },
                          inputTitle: 'Pilih Kelompok',
                          hint: '',
                        );
                      },
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(
                  horizontal: AppConstant.kDefaultPadding,
                  vertical: AppConstant.kDefaultPadding),
              width: double.infinity,
              child: CustomButton.solidPrimaryLarge(
                isActive: isValid,
                onTap: () async {
                  final confirmation = await showConfirmationDialog(
                      context: context,
                      title: 'Konfirmasi',
                      subtitle: widget.arguments.isEdit
                          ? 'Ubah bangunan?'
                          : 'Tambahkan bangunan?');
                  if (confirmation && context.mounted) {
                    if (widget.arguments.isEdit) {
                      context.read<EditBloc>().add(OnEditBangunan(
                          name: namaBangunanController.text,
                          noUrut: int.parse(noUrutBangunanController.text),
                          idKelompok: kelompok,
                          idBangunan: widget.arguments.bangunan.id ?? 0));
                    } else {
                      context.read<SubmitBloc>().add(OnSubmitBangunan(
                          name: namaBangunanController.text,
                          noUrut: int.parse(noUrutBangunanController.text),
                          idKelompok: kelompok));
                    }
                  }
                },
                text: 'Simpan',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
