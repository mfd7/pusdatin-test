import 'package:bangunan/domain/bangunan/usecases/submit_bangunan_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'submit_event.dart';
part 'submit_state.dart';

class SubmitBloc extends Bloc<SubmitEvent, SubmitState> {
  final SubmitBangunanUsecase _submitBangunanUsecase;
  SubmitBloc(this._submitBangunanUsecase) : super(SubmitInitial()) {
    on<OnSubmitBangunan>((event, emit) async {
      emit(SubmitLoading());
      final submit = await _submitBangunanUsecase.execute(
          name: event.name, noUrut: event.noUrut, idKelompok: event.idKelompok);
      submit.fold((failure) {
        emit(SubmitFailure(failure.message));
      }, (data) {
        emit(SubmitSuccess(data));
      });
    });
  }
}
