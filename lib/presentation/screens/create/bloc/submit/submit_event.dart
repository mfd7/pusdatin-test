part of 'submit_bloc.dart';

@immutable
abstract class SubmitEvent {}

class OnSubmitBangunan extends SubmitEvent {
  final String name;
  final int noUrut;
  final int idKelompok;

  OnSubmitBangunan(
      {required this.name, required this.noUrut, required this.idKelompok});
}
