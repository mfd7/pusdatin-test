part of 'submit_bloc.dart';

@immutable
abstract class SubmitState extends Equatable{

  @override
  List<Object?> get props => [];
}

class SubmitInitial extends SubmitState {}
class SubmitLoading extends SubmitState {}
class SubmitSuccess extends SubmitState {
  final String message;

  SubmitSuccess(this.message);

  @override
  List<Object?> get props => [message];
}
class SubmitFailure extends SubmitState {
  final String message;

  SubmitFailure(this.message);

  @override
  List<Object?> get props => [message];
}
