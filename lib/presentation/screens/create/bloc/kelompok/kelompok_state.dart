part of 'kelompok_bloc.dart';

@immutable
abstract class KelompokState extends Equatable {

  @override
  List<Object?> get props => [];
}

class KelompokInitial extends KelompokState {}
class KelompokLoading extends KelompokState {}
class KelompokLoaded extends KelompokState {
  final List<Kelompok> listKelompok;

  KelompokLoaded(this.listKelompok);

  @override
  List<Object?> get props => [listKelompok];
}
class KelompokFailure extends KelompokState {
  final String message;

  KelompokFailure(this.message);

  @override
  List<Object?> get props => [message];
}
