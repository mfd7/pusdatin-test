import 'package:bangunan/domain/bangunan/entities/kelompok.dart';
import 'package:bangunan/domain/bangunan/usecases/retrieve_list_kelompok_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'kelompok_event.dart';
part 'kelompok_state.dart';

class KelompokBloc extends Bloc<KelompokEvent, KelompokState> {
  final RetrieveListKelompokUsecase _kelompokUsecase;
  KelompokBloc(this._kelompokUsecase) : super(KelompokInitial()) {
    on<OnRetrieveListKelompok>((event, emit) async {
      emit(KelompokLoading());
      final retrieve = await _kelompokUsecase.execute();
      retrieve.fold((failure) {
        emit(KelompokFailure(failure.message));
      }, (data) {
        emit(KelompokLoaded(data));
      });
    });
  }
}
