part of 'edit_bloc.dart';

@immutable
abstract class EditEvent {}

class OnEditBangunan extends EditEvent {
  final String name;
  final int noUrut;
  final int idKelompok;
  final int idBangunan;

  OnEditBangunan(
      {required this.name,
      required this.noUrut,
      required this.idKelompok,
      required this.idBangunan});
}
