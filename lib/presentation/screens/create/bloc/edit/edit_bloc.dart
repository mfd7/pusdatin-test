import 'package:bangunan/domain/bangunan/usecases/submit_bangunan_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'edit_event.dart';
part 'edit_state.dart';

class EditBloc extends Bloc<EditEvent, EditState> {
  final SubmitBangunanUsecase _submitBangunanUsecase;
  EditBloc(this._submitBangunanUsecase) : super(EditInitial()) {
    on<OnEditBangunan>((event, emit) async {
      emit(EditLoading());
      final submit = await _submitBangunanUsecase.execute(
          name: event.name,
          noUrut: event.noUrut,
          idKelompok: event.idKelompok,
          isEdit: true,
          idBangunan: event.idBangunan);
      submit.fold((failure) {
        emit(EditFailure(failure.message));
      }, (data) {
        emit(EditSuccess(data));
      });
    });
  }
}
