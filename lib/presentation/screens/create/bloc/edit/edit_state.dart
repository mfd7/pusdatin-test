part of 'edit_bloc.dart';

@immutable
abstract class EditState extends Equatable {
  @override
  List<Object?> get props => [];
}

class EditInitial extends EditState {}

class EditLoading extends EditState {}

class EditSuccess extends EditState {
  final String message;

  EditSuccess(this.message);

  @override
  List<Object?> get props => [message];
}

class EditFailure extends EditState {
  final String message;

  EditFailure(this.message);

  @override
  List<Object?> get props => [message];
}
