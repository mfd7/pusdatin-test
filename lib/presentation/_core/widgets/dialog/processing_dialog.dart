import 'package:bangunan/presentation/_core/app_color.dart';
import 'package:bangunan/presentation/_core/app_text_style.dart';
import 'package:flutter/material.dart';

showProcessingDialog(
    {required BuildContext context, required String title, String? subtitle}) {
  showDialog<void>(
    context: context,
    useSafeArea: false,
    builder: (BuildContext context) {
      return Scaffold(
        backgroundColor: const Color.fromRGBO(0, 0, 0, 0.4),
        body: Padding(
          padding: const EdgeInsets.only(left: 63.0, right: 62.51),
          child: Center(
            child: Container(
              decoration: const BoxDecoration(
                color: AppColor.neutral1,
                borderRadius: BorderRadius.all(
                  Radius.circular(17.0),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(23.0, 30.82, 23.0, 30.0),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Stack(
                        children: [
                          CircularProgressIndicator(
                            backgroundColor: AppColor.celestialBlue,
                          ),
                          CircularProgressIndicator(
                            strokeWidth: 6.0,
                            backgroundColor: Colors.transparent,
                            valueColor: AlwaysStoppedAnimation<Color>(
                                AppColor.mediumPersianBlue),
                          ),
                        ],
                      ),
                      const SizedBox(height: 24),
                      Text(
                        title,
                        style: AppTextStyle.titleMedium,
                      ),
                      Visibility(
                        visible: subtitle != null,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 16.0),
                          child: Text(
                            subtitle ?? '',
                            style: AppTextStyle.bodyMedium,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ]),
              ),
            ),
          ),
        ),
      );
    },
  );
}
