import 'package:flutter/material.dart';

Future<bool> showConfirmationDialog(
    {required BuildContext context,
    required String title,
    required String subtitle}) async {
  var result = await showDialog<bool>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) => AlertDialog(
      title: Text(title),
      content: Text(subtitle),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.of(context).pop(true);
          },
          child: const Text('Ya'),
        ),
        TextButton(
          onPressed: () {
            Navigator.of(context).pop(false);
          },
          child: const Text('Tidak'),
        ),
      ],
    ),
  );
  return result ?? false;
}
