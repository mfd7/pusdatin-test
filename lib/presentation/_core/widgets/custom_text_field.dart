import 'package:flutter/material.dart';
import 'package:bangunan/_core/constants/app_constant.dart';
import 'package:bangunan/presentation/_core/app_color.dart';
import 'package:bangunan/presentation/_core/app_text_style.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatelessWidget {
  final String inputTitle;
  final String hintText;
  final String? secondHintText;
  final TextEditingController controller;
  final TextEditingController? secondController;
  final Widget? prefixIcon;
  final Widget? secondPrefixIcon;
  final Widget? suffixIcon;
  final Widget? secondSuffixIcon;
  final bool obscureText;
  final bool secondObscureText;
  final FocusNode focusNode;
  final FocusNode? secondFocusNode;
  final bool isFocus;
  final bool secondIsFocus;
  final bool isReadOnly;
  final bool secondIsReadOnly;
  final int maxLines;
  final void Function()? onTap;
  final void Function()? secondOnTap;
  final bool isError;
  final String errorMessage;
  final bool secondIsError;
  final void Function(String value)? onChanged;
  final void Function(String value)? secondOnChanged;
  final bool isImportant;
  final bool isLast;
  final bool secondIsLast;
  final bool useTwo;
  final bool useCheckBox;
  final bool useSecondCheckBox;
  final String? checkBoxText;
  final String? secondCheckBoxText;
  final Function(bool?)? checkBoxOnChanged;
  final Function(bool?)? secondCheckBoxOnChanged;
  final bool checkBoxValue;
  final bool secondCheckBoxValue;
  final TextInputType keyboardType;
  final List<TextInputFormatter> inputFormatters;

  const CustomTextField({
    super.key,
    required this.inputTitle,
    this.hintText = '',
    required this.controller,
    this.prefixIcon,
    this.suffixIcon,
    this.obscureText = false,
    required this.focusNode,
    required this.isFocus,
    this.isReadOnly = false,
    this.onTap,
    this.maxLines = 1,
    this.isError = false,
    this.onChanged,
    this.isImportant = false,
    this.isLast = false,
    this.useTwo = false,
    this.secondHintText,
    this.secondController,
    this.secondPrefixIcon,
    this.secondSuffixIcon,
    this.secondObscureText = false,
    this.secondFocusNode,
    this.secondIsFocus = false,
    this.secondIsReadOnly = false,
    this.secondOnTap,
    this.secondIsError = false,
    this.secondOnChanged,
    this.secondIsLast = false,
    this.useCheckBox = false,
    this.useSecondCheckBox = false,
    this.checkBoxText,
    this.secondCheckBoxText,
    this.checkBoxOnChanged,
    this.secondCheckBoxOnChanged,
    this.checkBoxValue = false,
    this.secondCheckBoxValue = false,
    this.errorMessage = '',
    this.keyboardType = TextInputType.text,
    this.inputFormatters = const [],
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text.rich(
          TextSpan(
            text: inputTitle,
            style: AppTextStyle.bodyLarge.copyWith(
              color: AppColor.neutral10,
              fontWeight: FontWeight.bold,
            ),
            children: [
              if (isImportant) ...[
                TextSpan(
                  text: ' *',
                  style: AppTextStyle.bodyLarge
                      .copyWith(color: AppColor.dangerNormal),
                ),
              ]
            ],
          ),
        ),
        const SizedBox(
          height: AppConstant.kDefaultPadding / 2,
        ),
        SizedBox(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Stack(
                      children: [
                        Positioned.fill(
                          child: AnimatedContainer(
                            duration: const Duration(seconds: 2),
                            decoration: isFocus
                                ? BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    boxShadow: [
                                        BoxShadow(
                                            color: AppColor.primary500
                                                .withOpacity(0.24),
                                            blurRadius: 0,
                                            spreadRadius: 4,
                                            offset: const Offset(0, 0)),
                                      ])
                                : null,
                          ),
                        ),
                        TextField(
                          controller: controller,
                          focusNode: focusNode,
                          onTap: onTap,
                          readOnly: isReadOnly,
                          textInputAction: isLast
                              ? TextInputAction.done
                              : TextInputAction.next,
                          decoration: InputDecoration(
                            hintText: hintText,
                            hintStyle: AppTextStyle.bodyMedium
                                .copyWith(color: AppColor.neutral7),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: BorderSide(
                                style: BorderStyle.solid,
                                color: AppColor.neutral7,
                                width: isError ? 2 : 1,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: const BorderSide(
                                style: BorderStyle.solid,
                                width: 2,
                                color: AppColor.primary500,
                              ),
                            ),
                            fillColor: AppColor.neutral1,
                            filled: true,
                            prefixIcon: prefixIcon,
                            suffixIcon: suffixIcon,
                            contentPadding: const EdgeInsets.all(12),
                            isDense: true,
                          ),
                          style: AppTextStyle.bodyMedium
                              .copyWith(color: AppColor.neutral9),
                          obscureText: obscureText,
                          obscuringCharacter: '*',
                          maxLines: maxLines,
                          onChanged: onChanged,
                          keyboardType: keyboardType,
                          inputFormatters: inputFormatters,
                        ),
                      ],
                    ),
                  ),
                  if (useTwo) ...[
                    const SizedBox(
                      width: AppConstant.kDefaultPadding / 2,
                    ),
                    Expanded(
                      child: Stack(
                        children: [
                          if (!secondIsError) ...[
                            Positioned.fill(
                              child: AnimatedContainer(
                                duration: const Duration(seconds: 2),
                                decoration: secondIsFocus && secondCheckBoxValue
                                    ? BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        boxShadow: [
                                            BoxShadow(
                                                color: AppColor.primary500
                                                    .withOpacity(0.24),
                                                blurRadius: 0,
                                                spreadRadius: 4,
                                                offset: const Offset(0, 0)),
                                          ])
                                    : null,
                              ),
                            ),
                          ],
                          TextField(
                            controller: secondController,
                            focusNode: secondFocusNode,
                            onTap: secondCheckBoxValue ? secondOnTap : null,
                            readOnly: secondIsReadOnly,
                            textInputAction: secondIsLast
                                ? TextInputAction.done
                                : TextInputAction.next,
                            decoration: InputDecoration(
                              hintText: secondHintText,
                              hintStyle: AppTextStyle.bodyMedium
                                  .copyWith(color: AppColor.neutral7),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8),
                                borderSide: BorderSide(
                                  style: BorderStyle.solid,
                                  color: secondIsError
                                      ? AppColor.dangerNormal
                                      : AppColor.neutral3,
                                  width: secondIsError ? 2 : 1,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8),
                                borderSide: BorderSide(
                                  style: BorderStyle.solid,
                                  width: 2,
                                  color: secondIsError
                                      ? AppColor.dangerNormal
                                      : AppColor.primary500,
                                ),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8),
                                borderSide: const BorderSide(
                                  style: BorderStyle.solid,
                                  color: AppColor.neutral3,
                                ),
                              ),
                              fillColor: secondCheckBoxValue
                                  ? AppColor.neutral1
                                  : AppColor.neutral3,
                              filled: true,
                              prefixIcon: secondPrefixIcon,
                              suffixIcon: secondSuffixIcon,
                              contentPadding: const EdgeInsets.all(12),
                              isDense: true,
                              enabled: secondCheckBoxValue,
                            ),
                            style: AppTextStyle.bodyMedium
                                .copyWith(color: AppColor.neutral9),
                            obscureText: secondObscureText,
                            obscuringCharacter: '*',
                            maxLines: maxLines,
                            onChanged: secondOnChanged,
                          ),
                        ],
                      ),
                    )
                  ]
                ],
              ),
              if (isError) ...[
                const SizedBox(
                  height: AppConstant.kDefaultPadding / 2,
                ),
                Text(
                  errorMessage,
                  style: AppTextStyle.labelMedium
                      .copyWith(color: AppColor.dangerNormal),
                )
              ],
            ],
          ),
        )
      ],
    );
  }
}
