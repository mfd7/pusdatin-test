import 'package:flutter/material.dart';
import 'package:bangunan/_core/constants/app_constant.dart';
import 'package:bangunan/presentation/_core/app_color.dart';
import 'package:bangunan/presentation/_core/app_text_style.dart';

enum CustomButtonSize { small, medium, large }

enum CustomButtonType { solid, flat, outlined, text }

class CustomButton extends StatelessWidget {
  final String? text;
  final Color textColor;
  final Widget? leading;
  final Widget? trailing;
  final Function()? onTap;
  final CustomButtonSize customButtonSize;
  final CustomButtonType customButtonType;
  final Color? color;
  final bool isActive;
  final bool useShadow;
  final double? horizontalPadding;
  final ValueNotifier<bool> tapNotifier = ValueNotifier<bool>(false);
  final bool dottedBorder;
  final Color? inActiveColor;
  final Color? inActiveTextColor;

  CustomButton({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.solid,
    this.color = AppColor.secondaryNormal,
    this.textColor = AppColor.secondaryDark,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.solidPrimaryLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.solid,
    this.color = AppColor.secondaryNormal,
    this.textColor = AppColor.secondaryDark,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.solidPrimaryMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.medium,
    this.customButtonType = CustomButtonType.solid,
    this.color = AppColor.secondaryNormal,
    this.textColor = AppColor.secondaryDark,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.solidPrimarySmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.solid,
    this.color = AppColor.secondaryNormal,
    this.textColor = AppColor.secondaryDark,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.flatPrimaryLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.flat,
    this.color = AppColor.primary50,
    this.textColor = AppColor.primary500,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.flatPrimaryMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.medium,
    this.customButtonType = CustomButtonType.flat,
    this.color = AppColor.primary50,
    this.textColor = AppColor.primary500,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.flatPrimarySmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.flat,
    this.color = AppColor.primary50,
    this.textColor = AppColor.primary500,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.flatWarningLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.flat,
    this.color = AppColor.warningNormal,
    this.textColor = AppColor.neutral1,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.flatWarningMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.medium,
    this.customButtonType = CustomButtonType.flat,
    this.color = AppColor.warningNormal,
    this.textColor = AppColor.neutral1,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.flatWarningSmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.flat,
    this.color = AppColor.warningNormal,
    this.textColor = AppColor.neutral1,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.flatDangerLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.flat,
    this.color = AppColor.dangerNormal,
    this.textColor = AppColor.neutral1,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.flatDangerMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.medium,
    this.customButtonType = CustomButtonType.flat,
    this.color = AppColor.dangerNormal,
    this.textColor = AppColor.neutral1,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.flatDangerSmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.flat,
    this.color = AppColor.dangerNormal,
    this.textColor = AppColor.neutral1,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.flatSuccessLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.flat,
    this.color = AppColor.successNormal,
    this.textColor = AppColor.neutral1,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.flatSuccessMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.medium,
    this.customButtonType = CustomButtonType.flat,
    this.color = AppColor.successNormal,
    this.textColor = AppColor.neutral1,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.flatSuccessSmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.flat,
    this.color = AppColor.successNormal,
    this.textColor = AppColor.neutral1,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.outlinedPrimaryLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.outlined,
    this.color = AppColor.neutral1,
    this.textColor = AppColor.secondaryNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.outlinedPrimaryMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.medium,
    this.customButtonType = CustomButtonType.outlined,
    this.color = AppColor.neutral1,
    this.textColor = AppColor.secondaryNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.outlinedPrimarySmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.outlined,
    this.color = AppColor.neutral1,
    this.textColor = AppColor.secondaryNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.outlinedWarningLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.outlined,
    this.color = AppColor.warningBase,
    this.textColor = AppColor.warningNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.outlinedWarningMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.medium,
    this.customButtonType = CustomButtonType.outlined,
    this.color = AppColor.warningBase,
    this.textColor = AppColor.warningNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.outlinedWarningSmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.outlined,
    this.color = AppColor.warningBase,
    this.textColor = AppColor.warningNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.outlinedDangerLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.outlined,
    this.color = AppColor.dangerBase,
    this.textColor = AppColor.dangerNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.outlinedDangerMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.medium,
    this.customButtonType = CustomButtonType.outlined,
    this.color = AppColor.dangerBase,
    this.textColor = AppColor.dangerNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.outlinedDangerSmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.outlined,
    this.color = AppColor.dangerBase,
    this.textColor = AppColor.dangerNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.outlinedSuccessLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.outlined,
    this.color = AppColor.successBase,
    this.textColor = AppColor.successNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.outlinedSuccessMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.outlined,
    this.color = AppColor.successBase,
    this.textColor = AppColor.successNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.outlinedSuccessSmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.outlined,
    this.color = AppColor.successBase,
    this.textColor = AppColor.successNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.textPrimaryLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.text,
    this.color,
    this.textColor = AppColor.secondaryNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.textPrimaryMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.medium,
    this.customButtonType = CustomButtonType.text,
    this.color,
    this.textColor = AppColor.secondaryNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.textPrimarySmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.text,
    this.color,
    this.textColor = AppColor.secondaryNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.textWarningLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.text,
    this.color,
    this.textColor = AppColor.warningNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.textWarningMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.medium,
    this.customButtonType = CustomButtonType.text,
    this.color,
    this.textColor = AppColor.warningNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.textWarningSmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.text,
    this.color,
    this.textColor = AppColor.warningNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.textDangerLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.text,
    this.color,
    this.textColor = AppColor.dangerNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.textDangerMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.medium,
    this.customButtonType = CustomButtonType.text,
    this.color,
    this.textColor = AppColor.dangerNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.textDangerSmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.text,
    this.color,
    this.textColor = AppColor.dangerNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.textSuccessLarge({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.large,
    this.customButtonType = CustomButtonType.text,
    this.color,
    this.textColor = AppColor.successNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.textSuccessMedium({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.medium,
    this.customButtonType = CustomButtonType.text,
    this.color,
    this.textColor = AppColor.successNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  CustomButton.textSuccessSmall({
    super.key,
    required this.onTap,
    this.text,
    this.customButtonSize = CustomButtonSize.small,
    this.customButtonType = CustomButtonType.text,
    this.color,
    this.textColor = AppColor.successNormal,
    this.leading,
    this.trailing,
    this.isActive = true,
    this.useShadow = true,
    this.horizontalPadding,
    this.dottedBorder = false,
    this.inActiveColor = AppColor.neutral6,
    this.inActiveTextColor = AppColor.neutral1,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: isActive ? onTap : null,
      onTapDown: isActive
          ? (details) {
              tapNotifier.value = true;
            }
          : null,
      onTapUp: isActive
          ? (details) {
              tapNotifier.value = false;
            }
          : null,
      child: ValueListenableBuilder<bool>(
        valueListenable: tapNotifier,
        builder: (context, isTapped, child) {
          return _CustomButton(
            isActive: isActive,
            customButtonType: customButtonType,
            color: color,
            textColor: textColor,
            useShadow: useShadow,
            horizontalPadding: horizontalPadding,
            customButtonSize: customButtonSize,
            leading: leading,
            text: text,
            trailing: trailing,
            isTapped: isTapped,
            dottedBorder: dottedBorder,
            inActiveColor: inActiveColor,
            inActiveTextColor: inActiveTextColor,
          );
        },
      ),
    );
  }
}

class _CustomButton extends StatelessWidget {
  const _CustomButton({
    required this.isActive,
    required this.customButtonType,
    required this.color,
    required this.textColor,
    required this.useShadow,
    required this.horizontalPadding,
    required this.customButtonSize,
    required this.leading,
    required this.text,
    required this.trailing,
    required this.isTapped,
    required this.dottedBorder,
    required this.inActiveColor,
    required this.inActiveTextColor,
  });

  final bool isActive;
  final CustomButtonType customButtonType;
  final Color? color;
  final Color textColor;
  final bool useShadow;
  final double? horizontalPadding;
  final CustomButtonSize customButtonSize;
  final Widget? leading;
  final String? text;
  final Widget? trailing;
  final bool isTapped;
  final bool dottedBorder;
  final Color? inActiveColor;
  final Color? inActiveTextColor;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
          color: customButtonType == CustomButtonType.solid
              ? isActive
                  ? !isTapped
                      ? color
                      : AppColor.secondaryPressed
                  : inActiveColor
              : customButtonType == CustomButtonType.flat
                  ? isActive
                      ? !isTapped
                          ? color
                          : AppColor.warningBase
                      : inActiveColor
                  : null,
          borderRadius: BorderRadius.circular(12),
          border: customButtonType == CustomButtonType.outlined && !dottedBorder
              ? Border.all(
                  width: 1.5,
                  color: isActive ? textColor : inActiveColor!,
                )
              : null,
          boxShadow: [
            if (isActive) ...[
              if (customButtonType == CustomButtonType.solid) ...[
                BoxShadow(
                  color: AppColor.neutral13.withOpacity(0.24),
                  blurRadius: 8.0,
                  offset: const Offset(4, 4),
                ),
              ],
              if (customButtonType == CustomButtonType.flat && useShadow) ...[
                BoxShadow(
                  color: AppColor.neutral13.withOpacity(isTapped ? 0.25 : 0.15),
                  blurRadius: isTapped ? 3.0 : 2.0,
                  offset: isTapped ? const Offset(0, 1) : const Offset(0, 2),
                ),
                if (isTapped) ...[
                  BoxShadow(
                    color: AppColor.neutral13.withOpacity(0.10),
                    blurRadius: 0,
                    offset: const Offset(0, -1),
                  ),
                ]
              ]
            ]
          ]),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: horizontalPadding == null
              ? AppConstant.kDefaultPadding / 2
              : horizontalPadding!,
          vertical: customButtonSize == CustomButtonSize.large
              ? AppConstant.kDefaultPadding / 8 * 7
              : AppConstant.kDefaultPadding / 8 * 5,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            if (leading != null) ...[
              leading!,
            ],
            if (leading != null && text != null) ...[
              const SizedBox(
                width: AppConstant.kDefaultPadding / 8 * 5,
              ),
            ],
            if (text != null) ...[
              Flexible(
                child: Text(
                  text!,
                  textAlign: TextAlign.left,
                  style: customButtonSize == CustomButtonSize.large
                      ? AppTextStyle.buttonLarge.copyWith(
                          color: isActive ? textColor : inActiveTextColor,
                          decoration: TextDecoration.none,
                        )
                      : customButtonSize == CustomButtonSize.medium
                          ? AppTextStyle.buttonMedium.copyWith(
                              color: isActive ? textColor : inActiveTextColor)
                          : AppTextStyle.buttonSmall.copyWith(
                              color: isActive ? textColor : inActiveTextColor,
                              decoration: TextDecoration.none,
                            ),
                ),
              ),
            ],
            if (trailing != null && text != null) ...[
              const SizedBox(
                width: AppConstant.kDefaultPadding / 8 * 5,
              ),
            ],
            if (trailing != null) ...[trailing!],
          ],
        ),
      ),
    );
  }
}
