import 'package:bangunan/_core/constants/app_constant.dart';
import 'package:flutter/material.dart';

showRegularModalBottomSheet(
    {required BuildContext context, required Widget content}) {
  showModalBottomSheet<void>(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16.0),
          topRight: Radius.circular(16.0),
        ),
      ),
      context: context,
      isScrollControlled: false,
      builder: (BuildContext context) {
        return SafeArea(
          child: Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: AppConstant.kDefaultPadding,
                  vertical: AppConstant.kDefaultPadding),
              child: content),
        );
      });
}
