import 'package:flutter/material.dart';
import 'package:bangunan/_core/constants/app_constant.dart';
import 'package:bangunan/presentation/_core/app_color.dart';
import 'package:bangunan/presentation/_core/app_text_style.dart';

class CustomDropdown extends StatelessWidget {
  final String? inputTitle;
  final Widget? prefixIcon;
  final FocusNode focusNode;
  final bool isFocus;
  final List<DropdownMenuItem> items;
  final dynamic selectedItem;
  final void Function(dynamic value) onChanged;
  final String hint;
  final bool isError;
  final bool isImportant;

  const CustomDropdown({
    Key? key,
    this.inputTitle,
    this.prefixIcon,
    required this.focusNode,
    required this.isFocus,
    required this.items,
    required this.selectedItem,
    required this.onChanged,
    required this.hint,
    this.isError = false,
    this.isImportant = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (inputTitle != null) ...[
          Text.rich(
            TextSpan(
              text: inputTitle,
              style: AppTextStyle.bodyLarge.copyWith(
                  color: AppColor.neutral10, fontWeight: FontWeight.bold),
              children: [
                if (isImportant) ...[
                  TextSpan(
                    text: ' *',
                    style: AppTextStyle.bodyLarge
                        .copyWith(color: AppColor.dangerNormal),
                  ),
                ]
              ],
            ),
          ),
          const SizedBox(
            height: AppConstant.kDefaultPadding / 2,
          ),
        ],
        Stack(
          children: [
            Positioned.fill(
              child: AnimatedContainer(
                duration: const Duration(seconds: 2),
                decoration: isFocus
                    ? BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                            BoxShadow(
                                color: AppColor.primary500.withOpacity(0.24),
                                blurRadius: 0,
                                spreadRadius: 4,
                                offset: const Offset(0, 0)),
                          ])
                    : null,
              ),
            ),
            DropdownButtonFormField(
              isDense: true,
              decoration: InputDecoration(
                prefixIcon: prefixIcon,
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                    style: BorderStyle.solid,
                    color: AppColor.neutral7,
                    width: isError ? 2 : 1,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(
                    style: BorderStyle.solid,
                    width: 2,
                    color: AppColor.primary500,
                  ),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                isDense: true,
                filled: true,
                fillColor: AppColor.neutral1,
                hintText: hint,
                hintStyle: AppTextStyle.bodyMedium.copyWith(
                  color: AppColor.neutral7,
                ),
              ),
              style: AppTextStyle.bodyMedium.copyWith(color: AppColor.neutral7),
              // icon: const Icon(Icons.arrow_drop_down),
              focusNode: focusNode,
              items: items,
              value: selectedItem,
              onChanged: onChanged,
            ),
          ],
        )
      ],
    );
  }
}
